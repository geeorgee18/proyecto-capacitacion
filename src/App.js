import React from 'react'
import {
  BrowserRouter,
  Switch,
  Route,
  Link
} from "react-router-dom"
import {Home} from './pages/Home'
import {Exercices} from './pages/Exercices'
const App = ()=> {
  return (
  <BrowserRouter>
    <Switch>
      <Route exact path='/' component={Home}/>
      <Route exact path='/ejercicio' component={Exercices}/>
    </Switch>
  </BrowserRouter>
  )
}

export default App
